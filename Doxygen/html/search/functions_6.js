var searchData=
[
  ['labyrinth',['Labyrinth',['../class_ojuvu_1_1_labyrinth.html#af62237e099b5c84b1f182c60d6a70d94',1,'Ojuvu.Labyrinth.Labyrinth(int time)'],['../class_ojuvu_1_1_labyrinth.html#a3d0b587aee48855408248d3d9f8869d8',1,'Ojuvu.Labyrinth.Labyrinth(LabyrinthViewModel lvm)']]],
  ['labyrinthviewmodel',['LabyrinthViewModel',['../class_ojuvu_1_1_labyrinth_view_model.html#a039191283e741112f8513f2762c11de2',1,'Ojuvu.LabyrinthViewModel.LabyrinthViewModel(int time, MazeGenerator myMaze)'],['../class_ojuvu_1_1_labyrinth_view_model.html#aa4e78bbf1f7b7cb6414658b36d28a563',1,'Ojuvu.LabyrinthViewModel.LabyrinthViewModel(char[,] board, Player player, Bomb bomb, Key key, BombCode bombCode, int timeLeft, Point exit)']]]
];
