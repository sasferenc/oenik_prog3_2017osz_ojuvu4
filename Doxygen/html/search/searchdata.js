var indexSectionsWithContent =
{
  0: "abcdefghiklmoprstwx",
  1: "abcgklmp",
  2: "ox",
  3: "abcgiklmops",
  4: "d",
  5: "lr",
  6: "bcdefghikptw",
  7: "p",
  8: "r"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "enums",
  5: "enumvalues",
  6: "properties",
  7: "events",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Enumerations",
  5: "Enumerator",
  6: "Properties",
  7: "Events",
  8: "Pages"
};

