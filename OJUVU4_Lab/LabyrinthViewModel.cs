﻿// <copyright file="LabyrinthViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Ojuvu
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// A labirintus megjelenítéséhez szükséges dolgokat tartalmazza
    /// </summary>
    public class LabyrinthViewModel : Bindable
    {
        private static Random rnd = new Random();

        private List<Point> freeCells;
        private Player player;
        private Bomb bomb;
        private Key key;
        private BombCode bombCode;
        private int timeLeft;

        /// <summary>
        /// Initializes a new instance of the <see cref="LabyrinthViewModel"/> class.
        /// A labirintus kialakítása
        /// </summary>
        /// <param name="time"> a felhasználó által megadott időkeret vagy a default idő</param>
        /// <param name="myMaze"> az újonnan generált labirintus</param>
        public LabyrinthViewModel(int time, MazeGenerator myMaze)
        {
            this.Board = myMaze.Grid;
            this.freeCells = myMaze.FreeCells;
            this.player = new Player(this.Placement());
            this.bomb = new Bomb(this.Placement());
            this.Key = new Key(this.Placement());
            this.Exit = new Point(0, (this.Board.GetLength(1) / 2) + 1);
            this.Board[(int)this.Exit.X, (int)this.Exit.Y] = 'E';
            this.BombCode = new BombCode(this.bomb.Code, this.Placement());
            this.TimeLeft = time;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LabyrinthViewModel"/> class.
        /// ha vissza akarjuk tölteni az elmentett játékot
        /// </summary>
        /// <param name="board">a pálya char[,] tömbben</param>
        /// <param name="player"> a játékos objektum</param>
        /// <param name="bomb"> a bomba</param>
        /// <param name="key"> a kijárat kulcsa</param>
        /// <param name="bombCode"> a bombához tartozó kód amit a felhasználó megtalálhat</param>
        /// <param name="timeLeft"> mennyi ideje maradt a felhasználónak </param>
        /// <param name="exit"> a kijárat</param>
        public LabyrinthViewModel(char[,] board, Player player, Bomb bomb, Key key, BombCode bombCode, int timeLeft, Point exit)
        {
            this.Board = board;
            this.player = player;
            this.bomb = bomb;
            this.key = key;
            this.BombCode = bombCode;
            this.TimeLeft = timeLeft;
            this.Exit = exit;
        }

        /// <summary>
        /// Gets or sets, a pálya, vagyis a falak és a kijárat
        /// </summary>
        public char[,] Board { get; set; }

        /// <summary>
        /// Gets or sets, a játékos képének frissitéséhez az onpropertychanged
        /// </summary>
        public Player Player
        {
            get
            {
                return this.player;
            }

            set
            {
                this.player = value;
                this.OnPropoertyChanged("Player");
                this.OnPropoertyChanged("PlayerBrush");
            }
        }

        /// <summary>
        /// Gets or sets, a bomba képének frissitéséhez az onpropertychanged
        /// </summary>
        public Bomb Bomb
        {
            get
            {
                return this.bomb;
            }

            set
            {
                this.bomb = value;
                this.OnPropoertyChanged("BombBrush");
            }
        }

        /// <summary>
        /// Gets or sets, a kijárat kulcsának képének frissitéséhez az onpropertychanged, továbbá az ajtó kinyitásához
        /// </summary>
        public Key Key
        {
            get
            {
                return this.key;
            }

            set
            {
                this.key = value;
                this.OnPropoertyChanged("KeyBrush");
                this.OnPropoertyChanged("ExitBrush");
            }
        }

        /// <summary>
        /// Gets or sets, a bomba kódjának képének frissitéséhez az onpropertychanged
        /// </summary>
        public BombCode BombCode
        {
            get
            {
                return this.bombCode;
            }

            set
            {
                this.bombCode = value;
                this.OnPropoertyChanged("BombCodeBrush");
            }
        }

        /// <summary>
        /// Gets or sets kijárat
        /// </summary>
        public Point Exit { get; set; }

        /// <summary>
        /// Gets a player karakterének megjelenítéséhez brush
        /// </summary>
        public Brush PlayerBrush
        {
            get
            {
                return this.Player.Direction == Direction.Right ? new ImageBrush(new BitmapImage(new Uri(@"pictures\player.png", UriKind.Relative))) :
                 new ImageBrush(new BitmapImage(new Uri(@"pictures\playerleft.png", UriKind.Relative)));
            }
        }

        /// <summary>
        /// Gets a falak megjelenítéséhez brush
        /// </summary>
        public Brush WallBrush
        {
            get { return new ImageBrush(new BitmapImage(new Uri(@"pictures\wall.jpg", UriKind.Relative))); }
        }

        /// <summary>
        /// Gets a bomba megjelenítéséhez brush
        /// </summary>
        public Brush BombBrush
        {
            get
            {
                return this.bomb.IsVisited == false ? new ImageBrush(new BitmapImage(new Uri(@"pictures\box.png", UriKind.Relative))) :
                new ImageBrush(new BitmapImage(new Uri(@"pictures\bomb.png", UriKind.Relative)));
            }
        }

        /// <summary>
        /// Gets a kulcs megjelenítéséhez brush
        /// </summary>
        public Brush KeyBrush
        {
            get
            {
                return this.key.IsVisited == false ? new ImageBrush(new BitmapImage(new Uri(@"pictures\box.png", UriKind.Relative))) :
                null;
            }
        }

        /// <summary>
        /// Gets a bomba kódjának megjelenítéséhez brush
        /// </summary>
        public Brush BombCodeBrush
        {
            get
            {
                return this.BombCode.IsVisited == false ? new ImageBrush(new BitmapImage(new Uri(@"pictures\box.png", UriKind.Relative))) :
                null;
            }
        }

        /// <summary>
        /// Gets a kijárat kódjának megjelenítéséhez brush
        /// </summary>
        public Brush ExitBrush
        {
            get
            {
                return this.Key.IsVisited == false ? new ImageBrush(new BitmapImage(new Uri(@"pictures\cd.png", UriKind.Relative))) :
                null;
            }
        }

        /// <summary>
        /// Gets or sets időkerethez
        /// /// </summary>
        public int TimeLeft
        {
            get
            {
                return this.timeLeft;
            }

            set
            {
                this.timeLeft = value;
                this.OnPropoertyChanged("TimeLeft");
            }
        }

        /// <summary>
        /// Gets a value indicating whether, engedélyezett e a beírt kódot ellneőrző gomb
        /// </summary>
        public bool BombCodeEnabled
        {
            get { return this.Bomb.Coordinates == this.Player.Coordinates; }
        }

        private Point Placement()
        {
            int rand = rnd.Next(0, this.freeCells.Count);
            Point koord = this.freeCells[rand];
            this.freeCells.Remove(koord);
            return koord;
        }
    }
}
