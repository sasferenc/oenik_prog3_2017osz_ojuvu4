﻿// <copyright file="GameObject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Ojuvu
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// a játéktéren megjelenítendő eszközök,játékos őse
    /// </summary>
    public abstract class GameObject : Bindable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameObject"/> class.
        /// a koordinátáit adja meg az objektumnak a konstruktorban
        /// </summary>
        /// <param name="coordinates">objektum koordinátái</param>
        protected GameObject(Point coordinates)
        {
            this.Coordinates = new Point(coordinates.X, coordinates.Y);
        }

        /// <summary>
        /// Gets or sets, a koordinátákat adja vissza vagy állítja be
        /// </summary>
        public Point Coordinates { get; set; }
    }
}
