﻿// <copyright file="Key.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Ojuvu
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// a kijáratot nyitó kulcs
    /// </summary>
    public class Key : GameObjectWithIsVisited
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Key"/> class.
        /// a kulcs amit ha megtalál a játékos akkor eltűnik a doboza, és ki tud menni a labirintusból
        /// </summary>
        /// <param name="p"> a koordinátája a kulcsnak</param>
        public Key(Point p)
            : base(p, false)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Key"/> class.
        /// már meglévő kulcsot módosítunkv vagy pedig visszatöltésnél adja meg neki a megfelelő értékeket a konstruktor
        /// </summary>
        /// <param name="p"> a kulcs koordinátái</param>
        /// <param name="visited"> a kulcsot megtalálta a már a játékos</param>
        public Key(Point p, bool visited)
            : base(p, visited)
        {
        }
    }
}
