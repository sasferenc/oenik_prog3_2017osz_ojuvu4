﻿// <copyright file="BombCode.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Ojuvu
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// a bombkódot megvalósító osztály
    /// </summary>
    public class BombCode : GameObjectWithIsVisited
    {
        private string code;

        /// <summary>
        /// Initializes a new instance of the <see cref="BombCode"/> class.
        /// </summary>
        /// <param name="code">a bomba kódja</param>
        /// <param name="coordinates"> a bomba koordinátái</param>
        public BombCode(int code, Point coordinates)
            : base(coordinates)
        {
            this.Code = code.ToString();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BombCode"/> class.
        /// mentett játéknál állítja be, vagy ha meglátogattuk a játékossal
        /// </summary>
        /// <param name="coordinates">a koordináták</param>
        /// <param name="isVisited">járt e már itt a játékos</param>
        /// <param name="code"> a bomba kódja</param>
        public BombCode(Point coordinates, bool isVisited, string code)
            : base(coordinates, isVisited)
        {
            this.Code = code;
        }

        /// <summary>
        /// Gets or sets, a textboxhoz attól függően hogy a játékos megtalátla e már a bombakódot
        /// </summary>
        public string Code
        {
            get
            {
                return this.IsVisited == true ? this.code : "***";
            }

            set
            {
                this.code = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether megtalálta e már a játékos
        /// </summary>
        public new bool IsVisited
        {
            get
            {
                return base.IsVisited;
            }

            set
            {
                base.IsVisited = value;
                this.OnPropoertyChanged("Code");
            }
        }
    }
}
