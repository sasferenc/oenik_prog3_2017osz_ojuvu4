﻿// <copyright file="GameObjectWithIsVisited.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Ojuvu
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// olyan game objectek amelyek rendelkeznek isvisited tulajdonsággal
    /// </summary>
    public abstract class GameObjectWithIsVisited : GameObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameObjectWithIsVisited"/> class.
        /// itt állítódik a visited tulajdonság
        /// </summary>
        /// <param name="coordinates">az objektum koordinátái lesz</param>
        /// <param name="isVisited"> az objektumot megtalálta-e már a játékos</param>
        protected GameObjectWithIsVisited(Point coordinates, bool isVisited)
            : base(coordinates)
        {
            this.IsVisited = isVisited;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameObjectWithIsVisited"/> class.
        /// amikor először létrehozunk egy olyan objektumot aminek van visited tulajdonsga akkor azt automatikusan hamisra állítja
        /// </summary>
        /// <param name="coordinates">az objektum koordinátái lesz</param>
        protected GameObjectWithIsVisited(Point coordinates)
            : base(coordinates)
        {
            this.IsVisited = false;
        }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// a játékos megtalálta e már az objektumot
        /// </summary>
        public bool IsVisited { get; set; }
    }
}
