﻿// <copyright file="Bomb.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Ojuvu
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// a játék bombáját replezentáló osztály
    /// </summary>
    public class Bomb : GameObjectWithIsVisited
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Bomb"/> class.
        /// a bomba osztály kontruktora ahol új bombát hozunk létre
        /// </summary>
        /// <param name="coordinates">a bomba koordinátái</param>
        public Bomb(Point coordinates)
            : base(coordinates, false)
        {
            Random rnd = new Random();
            this.Code = rnd.Next(100, 1000);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Bomb"/> class.
        /// egy mentett játék visszállításánál használjuk, illetve amikor frissíteni kell a látogatottságot
        /// </summary>
        /// <param name="coordinates"> a bomba koordinátái</param>
        /// <param name="isVisited"> a játékos megtalálta-e már a bombát</param>
        /// <param name="code"> a bomba kódja </param>
        public Bomb(Point coordinates, bool isVisited, int code)
            : base(coordinates, isVisited)
        {
            this.Code = code;
        }

        /// <summary>
        /// Gets or sets, a kód amivel hatástalanítható a bomba
        /// </summary>
        public int Code { get; set; }
    }
}
