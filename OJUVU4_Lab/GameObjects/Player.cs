﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Ojuvu
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// a játékos irányának jelzéséhez
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// bal irány
        /// </summary>
        Left,

        /// <summary>
        /// jobb irány
        /// </summary>
        Right
    }

    /// <summary>
    /// a játkost reprezentáló osztály
    /// </summary>
    public class Player : GameObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// a konstuktor ami beállítja a haskey értékét amikor legelőször hozom létre a játékost
        /// </summary>
        /// <param name="coordinates">a játékos koordinátái</param>
        public Player(Point coordinates)
            : base(coordinates)
        {
            this.HasKey = false;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// visszaállításnál hozza létre a játékost
        /// </summary>
        /// <param name="coordinates"> a játékos koordinátái</param>
        /// <param name="haskey"> a játékos megtalálta már-e a kijárathoz vezető kulcsot</param>
        /// <param name="direction"> a játékos merre néz</param>
        public Player(Point coordinates, bool haskey, Direction direction)
            : base(coordinates)
        {
            this.HasKey = haskey;
            this.Direction = direction;
        }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// a játkos megtalálta-e már a kijáratot nyitó kulcsot
        /// </summary>
        public bool HasKey { get; set; }

        /// <summary>
        /// Gets or sets a játékos aktuális iránya
        /// </summary>
        public Direction Direction { get; set; }
    }
}
