﻿// <copyright file="MazeGenerator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Ojuvu
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// a labirintus "vázának" illetve a szabad cellák megadásáért felelős
    /// </summary>
    public class MazeGenerator
    {
        private static Random random = new Random(); // The random object

        private int dimensionX;
        private int dimensionY;
        private int gridDimensionX;
        private int gridDimensionY;
        private char[,] grid;
        private Cell[,] cells;
        private List<Point> freecells = new List<Point>();

        /// <summary>
        /// Initializes a new instance of the <see cref="MazeGenerator"/> class.
        /// </summary>
        /// <param name="xDimension">A labirintusban hány "szoba" legyen vertikálisan egymás mellett</param>
        /// <param name="yDimension"> A labirintusban egymás alatt levő szbák száma</param>
        public MazeGenerator(int xDimension, int yDimension)
        {
            this.dimensionX = xDimension;
            this.dimensionY = yDimension;
            this.gridDimensionX = (xDimension * 2) + 1;
            this.gridDimensionY = (yDimension * 2) + 1;
            this.grid = new char[this.gridDimensionX, this.gridDimensionY];
            this.Init();
            this.Generate();
            this.Draw();
        }

        /// <summary>
        /// Gets or sets, labirintus képe levetítve egy character tömbbe, x a fal,e a kijárat de az csak később kerül bele
        /// </summary>
        public char[,] Grid
        {
            get { return this.grid; }
            set { this.grid = value; }
        }

        /// <summary>
        /// Gets visszaadja azokat a helyeket ahol nem fal helyezkedik el
        /// </summary>
        public List<Point> FreeCells
        {
            get { return this.freecells; } private set { this.freecells = value; }
        }

        private void Init()
        {
            this.cells = new Cell[this.dimensionX, this.dimensionY];
            for (int x = 0; x < this.dimensionX; x++)
            {
                for (int y = 0; y < this.dimensionY; y++)
                {
                    this.cells[x, y] = new Cell(x, y);
                }
            }
        }

        private void Generate()
        {
            this.GenerateMaze(this.GetCell(0, 0));
        }

        private void GenerateMaze(Cell startAt)
        {
            if (startAt == null)
            {
                return;
            }

            startAt.Visited = true;
            Stack<Cell> cells = new Stack<Cell>();
            cells.Push(startAt);
            while (!(cells.Count == 0))
            {
                Cell cell;
                cell = cells.Pop();

                List<Cell> neighbors = new List<Cell>();

                Cell[] potentialNeighbors = new Cell[]
                {
                        this.GetCell(cell.X + 1, cell.Y),
                        this.GetCell(cell.X, cell.Y + 1),
                        this.GetCell(cell.X - 1, cell.Y),
                        this.GetCell(cell.X, cell.Y - 1)
                };
                foreach (Cell other in potentialNeighbors)
                {
                    if (other == null || other.Visited)
                    {
                        continue;
                    }

                    neighbors.Add(other);
                }

                if (neighbors.Count == 0)
                {
                    continue;
                }

                Cell selected = neighbors[random.Next(neighbors.Count)];

                selected.Visited = true;
                cell.AddNeighbor(selected);
                cells.Push(cell);
                cells.Push(selected);
            }
        }

        private Cell GetCell(int x, int y)
        {
            if (x >= 0 && x < this.cells.GetLength(0) && y >= 0 && y < this.cells.GetLength(1))
            {
                return this.cells[x, y];
            }

            return null;
        }

        private void Draw()
        {
            char wallChar = 'X', cellChar = ' ';

            for (int x = 0; x < this.gridDimensionX; x++)
            {
                for (int y = 0; y < this.gridDimensionY; y++)
                {
                    if (x % 2 == 0 || y % 2 == 0)
                    {
                        this.grid[x, y] = wallChar;
                    }
                    else
                    {
                        this.freecells.Add(new Point(y, x));
                    }
                }
            }

            for (int x = 0; x < this.dimensionX; x++)
            {
                for (int y = 0; y < this.dimensionY; y++)
                {
                    Cell current = this.GetCell(x, y);
                    int gridX = (x * 2) + 1, gridY = (y * 2) + 1;

                    if (current.IsCellBelowNeighbor(this.GetCell(current.X, current.Y + 1)))
                    {
                        this.grid[gridX, gridY + 1] = cellChar;
                        this.freecells.Add(new Point(gridY + 1, gridX));
                    }

                    if (current.IsCellRightNeighbor(this.GetCell(current.X + 1, current.Y)))
                    {
                        this.grid[gridX + 1, gridY] = cellChar;
                        this.freecells.Add(new Point(gridY, gridX + 1));
                    }
                }
            }
        }

        /// <summary>
        /// egy belső class amely egy cellát/szobát jelképezne amelynek vannak szomszédai és egyéb tulajdonságai
        /// </summary>
        private class Cell
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Cell"/> class.
            /// </summary>
            /// <param name="x"> a cella x koordinátája </param>
            /// <param name="y"> a cella y koordinátája </param>
            public Cell(int x, int y)
            {
                this.X = x;
                this.Y = y;
                this.Neighbors = new List<Cell>();
                this.Visited = false;
            }

            /// <summary>
            /// Gets or sets, a cella x kkordinátája
            /// </summary>
            public int X { get; set; }

            /// <summary>
            /// Gets or sets, a cella Y koordinátája
            /// </summary>
            public int Y { get; set; }

            /// <summary>
            /// Gets or sets, azokat a cellákat tartalmazza amelyekhez el lehet jutni az adott cellából (nem feltétlenül az összeset)
            /// </summary>
            public List<Cell> Neighbors { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether, az adott cellába eljutottunk-e már valahonnan
            /// </summary>
            public bool Visited { get; set; }

            /// <summary>
            /// Hozzáadja a cellához a szomszédot, és a szomszéd szomszédjaihoz is ezt a cellát
            /// </summary>
            /// <param name="other">a cella szomszédja</param>
            public void AddNeighbor(Cell other)
            {
                if (!this.Neighbors.Contains(other))
                {
                    this.Neighbors.Add(other);
                }

                if (!other.Neighbors.Contains(this))
                {
                    other.Neighbors.Add(this);
                }
            }

            /// <summary>
            /// Megmondja egy celláról hogy az hogyaz az adott cellának alsó szomszédja-e
            /// </summary>
            /// <param name="cell"> el lehet-e jutni az adott cellából az alattalevőbe</param>
            /// <returns>attól függően hogy szerepel-e a szomszédai között igaz, vagy hamis</returns>
            public bool IsCellBelowNeighbor(Cell cell)
            {
                return this.Neighbors.Contains(cell);
            }

            /// <summary>
            /// Megmondja egy celláról hogy az hogyaz az adott cellának alsó szomszédja-e
            /// </summary>
            /// <param name="cell"> el lehet-e jutni az adott cellából az mellettelevőbe </param>
            /// <returns>igen/nem</returns>
            public bool IsCellRightNeighbor(Cell cell)
            {
                return this.Neighbors.Contains(cell);
            }
        }
    }
}
