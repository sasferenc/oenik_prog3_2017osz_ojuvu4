﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Ojuvu
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using Ojuvu;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Labyrinth lab;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// A main windows business logic
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// itt ha egy a legutóbb mentett játékot szeretném visszatölteni akkor azt ez végzi el, itt beolvassuk a fájlból (ha létezik), és átadjuk az új ablaknak hogy ez legyena  datacontextje. Ha nem létezik jelezzük hogy még nem volt mentett játék
        /// </summary>
        /// <param name="sender">a gomb</param>
        /// <param name="e">egy routed eventargs</param>
        private void Button_Saved(object sender, RoutedEventArgs e)
        {
            try
            {
                List<string> save = new List<string>();
                StreamReader sr = null;
                using (sr = new StreamReader("save.txt"))
                {
                    while (!sr.EndOfStream)
                    {
                        save.Add(sr.ReadLine());
                    }
                }

                string aktualis = string.Empty;
                int idx = 0;
                while (aktualis != "palya_vege")
                {
                    aktualis = save[idx];
                    idx++;
                }

                idx--;
                char[,] board = new char[idx, save[0].Length];
                for (int i = 0; i < idx; i++)
                {
                    aktualis = save[0];
                    for (int j = 0; j < save[0].Length; j++)
                    {
                        board[i, j] = aktualis[j];
                    }

                    save.RemoveAt(0);
                }

                save.RemoveAt(0);
                string[] splitted = save[0].Split(',');
                Direction d = splitted[3] == "Left" ? Direction.Left : Direction.Right;
                Player player = new Player(new Point(int.Parse(splitted[0]), int.Parse(splitted[1])), bool.Parse(splitted[2]), d);
                save.RemoveAt(0);
                splitted = save[0].Split(',');
                Bomb bomb = new Bomb(new Point(int.Parse(splitted[0]), int.Parse(splitted[1])), bool.Parse(splitted[2]), int.Parse(splitted[3]));
                save.RemoveAt(0);
                splitted = save[0].Split(',');
                Ojuvu.Key key = new Ojuvu.Key(new Point(int.Parse(splitted[0]), int.Parse(splitted[1])), bool.Parse(splitted[2]));
                save.RemoveAt(0);
                splitted = save[0].Split(',');
                BombCode bc = new BombCode(new Point(int.Parse(splitted[0]), int.Parse(splitted[1])), bool.Parse(splitted[2]), splitted[3]);
                save.RemoveAt(0);
                int ido = int.Parse(save[0]);
                save.RemoveAt(0);
                splitted = save[0].Split(',');
                Point exit = new Point(int.Parse(splitted[0]), int.Parse(splitted[1]));
                this.lab = new Labyrinth(new LabyrinthViewModel(board, player, bomb, key, bc, ido, exit));
                this.Visibility = Visibility.Collapsed;
                this.lab.WindowState = WindowState.Maximized;
                this.lab.ShowDialog();
                this.Visibility = Visibility.Visible;
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Még nincs elmentett játék", "Hiba", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void New_Game(object sender, RoutedEventArgs e)
        {
            int time;
            if (this.textBox.Text == string.Empty)
            {
                time = 120;
            }
            else if (int.Parse(this.textBox.Text) > 0)
            {
                time = int.Parse(this.textBox.Text);
            }
            else
            {
                return;
            }

            this.lab = new Labyrinth(time);
            this.Visibility = Visibility.Collapsed;
            this.lab.WindowState = WindowState.Maximized;
            this.lab.ShowDialog();
            this.Visibility = Visibility.Visible;
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
            {
                e.Handled = true;
            }
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
