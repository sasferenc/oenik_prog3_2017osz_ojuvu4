﻿// <copyright file="Labyrinth.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Ojuvu
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using System.Windows.Threading;

    /// <summary>
    /// Interaction logic for Labyrinth.xaml
    /// </summary>
    public partial class Labyrinth : Window
    {
        private LabyrinthViewModel lVM;
        private DispatcherTimer timer;

        /// <summary>
        /// Initializes a new instance of the <see cref="Labyrinth"/> class.
        /// legenerálj a labirintust és átadja a wiev modelnek létrehozza a timert
        /// </summary>
        /// <param name="time">a felasználó által megadott idő vagy az alap 120 sec</param>
        public Labyrinth(int time)
        {
            this.InitializeComponent();
            this.DataContext = this.lVM = new LabyrinthViewModel(time, new MazeGenerator(10, 16));
            this.timer = new DispatcherTimer();
            this.textBox.Focusable = false;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Labyrinth"/> class.
        /// ha mentett játékot állítunk vissza
        /// </summary>
        /// <param name="lvm">a mentett játék wiev modelje</param>
        public Labyrinth(LabyrinthViewModel lvm)
        {
            this.InitializeComponent();
            this.DataContext = this.lVM = lvm;
            this.timer = new DispatcherTimer();
            this.textBox.Focusable = false;
        }

        /// <summary>
        /// ha az image van fókuszálva akkor lépteti a karaktert ha tudja
        /// </summary>
        /// <param name="sender">key</param>
        /// <param name="e"> melyik billentyű lett lenyomva</param>
        private void Iboard_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Right)
            {
                this.Move(1, 0);
            }
            else if (e.Key == System.Windows.Input.Key.Left)
            {
                this.Move(-1, 0);
            }
            else if (e.Key == System.Windows.Input.Key.Up)
            {
                this.Move(0, -1);
            }
            else if (e.Key == System.Windows.Input.Key.Down)
            {
                this.Move(0, 1);
            }

            this.check2.GetBindingExpression(Button.IsEnabledProperty).UpdateTarget();
        }

        /// <summary>
        /// mozgatja a játékost ha lehet
        /// </summary>
        /// <param name="i"> vízszintes irányba lépés</param>
        /// <param name="j"> függőlegesen lépés</param>
        private void Move(int i, int j)
        {
            int newX = (int)this.lVM.Player.Coordinates.X + i;
            int newY = (int)this.lVM.Player.Coordinates.Y + j;
            Direction d = i == 0 ? this.lVM.Player.Direction : (i == 1 ? Direction.Right : Direction.Left);
            if (this.lVM.Board[newY, newX] == 'E' && this.lVM.Player.HasKey)
            {
                this.lVM.Player = new Player(new Point(newX, newY), this.lVM.Player.HasKey, d);
            }
            else if ((this.lVM.Board[newY, newX] != 'E')
                && this.lVM.Board[newY, newX] != 'X')
            {
                this.lVM.Player = new Player(new Point(newX, newY), this.lVM.Player.HasKey, d);
            }
            else
            {
                return;
            }

            if (this.lVM.Player.Coordinates.Y == this.lVM.Exit.X && this.lVM.Player.Coordinates.X == this.lVM.Exit.Y && this.lVM.Player.HasKey == true)
            {
                MessageBox.Show("Nyertél");
                this.Close();
            }
            else if (this.lVM.Player.Coordinates == this.lVM.Bomb.Coordinates && !this.lVM.Bomb.IsVisited)
            {
                this.lVM.Bomb = new Bomb(this.lVM.Bomb.Coordinates, true, this.lVM.Bomb.Code);
            }
            else if (this.lVM.Player.Coordinates == this.lVM.Key.Coordinates && !this.lVM.Key.IsVisited)
            {
                this.lVM.Key = new Key(this.lVM.Key.Coordinates, true);
                this.lVM.Player.HasKey = true;
            }
            else if (this.lVM.Player.Coordinates == this.lVM.BombCode.Coordinates && !this.lVM.BombCode.IsVisited)
            {
                this.lVM.BombCode = new BombCode(this.lVM.BombCode.Coordinates, true, this.lVM.Bomb.Code.ToString());
                this.Code.GetBindingExpression(Label.ContentProperty).UpdateTarget();
            }
        }

        /// <summary>
        /// ha kattintunk a játéktérre akkor ez kerüljön fókuszba és a textboxot fókuszálhatatlanná tesszük
        /// </summary>
        /// <param name="sender"> az image</param>
        /// <param name="e"> a kattintás</param>
        private void Iboard_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.textBox.Focusable = false;
            this.Iboard.Focus();
        }

        /// <summary>
        /// miután betöltött az ablak mi történjen, elindítjuk, beállítjuk a timert feliratkozunk image keydown eseményére
        /// </summary>
        /// <param name="sender"> ablak</param>
        /// <param name="e"> eventargs</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Iboard.Focusable = true;
            this.Iboard.Focus();
            this.Iboard.KeyDown += this.Iboard_KeyDown;
            this.timer.Interval = new TimeSpan(0, 0, 0, 1);
            this.timer.Tick += this.Timer_Tick;
            this.timer.Start();
        }

        /// <summary>
        /// másodpercenként meghívódik és csökkenti az időt illetve vizsgálja hogy lejárt-e
        /// </summary>
        /// <param name="sender">timer</param>
        /// <param name="e">eventarg</param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            this.lVM.TimeLeft--;
            if (this.lVM.TimeLeft == 0)
            {
                MessageBox.Show("Lejárt az időd vesztettél!");
                this.timer.Stop();
                this.Close();
            }
        }

        /// <summary>
        /// a save gomb lenyomásakor elmenti a játék állapotát egy .txt file-ba
        /// </summary>
        /// <param name="sender">button click</param>
        /// <param name="e">routedeventargs</param>
        private void Button_Save(object sender, RoutedEventArgs e)
        {
            StreamWriter sw;
            using (sw = new StreamWriter("save.txt"))
            {
                string palyasor = string.Empty;
                for (int i = 0; i < this.lVM.Board.GetLength(0); i++)
                {
                    for (int j = 0; j < this.lVM.Board.GetLength(1); j++)
                    {
                        palyasor += this.lVM.Board[i, j];
                    }

                    sw.WriteLine(palyasor);
                    palyasor = string.Empty;
                }

                sw.WriteLine("palya_vege");
                Player temp = this.lVM.Player;
                sw.WriteLine("{0},{1},{2},{3},Player", temp.Coordinates.X, temp.Coordinates.Y, temp.HasKey, temp.Direction.ToString());
                Bomb tempB = this.lVM.Bomb;
                sw.WriteLine("{0},{1},{2},{3},Bomb", tempB.Coordinates.X, tempB.Coordinates.Y, tempB.IsVisited, tempB.Code);
                Key key = this.lVM.Key;
                sw.WriteLine("{0},{1},{2},Key", key.Coordinates.X, key.Coordinates.Y, key.IsVisited);
                BombCode bc = this.lVM.BombCode;
                sw.WriteLine("{0},{1},{2},{3},BombCode", bc.Coordinates.X, bc.Coordinates.Y, bc.IsVisited, tempB.Code);
                sw.WriteLine(this.lVM.TimeLeft);
                Point exit = this.lVM.Exit;
                sw.WriteLine("{0},{1}", exit.X, exit.Y);

                MessageBox.Show("Sikeres mentés");
            }
        }

        /// <summary>
        /// megnézi hogy jó kódot adtunk-e meg a bomba deaktiválásakkor
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">reoutedeventarg</param>
        private void Button_Check(object sender, RoutedEventArgs e)
        {
            if (this.textBox.Text == this.lVM.Bomb.Code.ToString())
            {
                MessageBox.Show("Nyertél");
                this.timer.Stop();
                this.Close();
            }
            else
            {
                MessageBox.Show("Rossz kódot adtál meg, a bomba felrobbant!\nVége a játéknak!", "Vesztettél!", MessageBoxButton.OK, MessageBoxImage.Warning);
                this.timer.Stop();
                this.Close();
            }
        }

        /// <summary>
        /// átfókuszál a textboxra
        /// </summary>
        /// <param name="sender">mouse</param>
        /// <param name="e">mousebutton</param>
        private void TextBox_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.textBox.Focusable = true;
            this.textBox.Focus();
        }
    }
}
