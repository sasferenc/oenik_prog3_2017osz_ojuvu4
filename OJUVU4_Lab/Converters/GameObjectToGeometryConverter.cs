﻿// <copyright file="GameObjectToGeometryConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Ojuvu
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// game objectből csinál négyzetet
    /// </summary>
    public class GameObjectToGeometryConverter : IValueConverter
    {
        /// <summary>
        /// game objectből csinál négyzetet
        /// </summary>
        /// <param name="value">a megjelenítendő game object</param>
        /// <param name="targetType">tergettype</param>
        /// <param name="parameter">paraméterek</param>
        /// <param name="culture">CultureInfo</param>
        /// <returns>egy négyzet amit megjeleníthet</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            GameObject go = (GameObject)value;

            return new RectangleGeometry(new Rect(go.Coordinates.X * 10, go.Coordinates.Y * 10, 10, 10));
        }

        /// <summary>
        /// convertback
        /// </summary>
        /// <param name="value">value</param>
        /// <param name="targetType">targettype</param>
        /// <param name="parameter">paraméterek</param>
        /// <param name="culture">Culture Info</param>
        /// <returns>error</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
