﻿// <copyright file="CharToGeometryConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Ojuvu
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// a character tömmből készít rectangle-ket
    /// </summary>
    public class CharToGeometryConverter : IValueConverter
    {
        /// <summary>
        /// a character tömmből készít rectangle-ket
        /// </summary>
        /// <param name="value">a character tömb</param>
        /// <param name="targetType">targetType</param>
        /// <param name="parameter">paraméterek</param>
        /// <param name="culture">CultureInfo</param>
        /// <returns>geometry group ami a falakat tartalmazza</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            char[,] board = (char[,])value;
            GeometryGroup gg = new GeometryGroup();

            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    if (board[i, j] == 'X')
                    {
                        gg.Children.Add(new RectangleGeometry(new Rect(j * 10, i * 10, 10, 10)));
                    }
                }
            }

            return gg;
        }

        /// <summary>
        /// geometry groupból csinálna char[,] tömböt
        /// </summary>
        /// <param name="value">geometry group</param>
        /// <param name="targetType">parameterType</param>
        /// <param name="parameter">paraméterek</param>
        /// <param name="culture">CultureInfo</param>
        /// <returns>error</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
