﻿// <copyright file="PointToGeometryComverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Ojuvu
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// a pontokból csinál egy négyzetet
    /// </summary>
    public class PointToGeometryComverter : IValueConverter
    {
        /// <summary>
        /// pontot konvertálja négyzetté
        /// </summary>
        /// <param name="value"> a pont</param>
        /// <param name="targetType">targettype</param>
        /// <param name="parameter">egyéb paraméter lehet</param>
        /// <param name="culture"> culture info</param>
        /// <returns>egy négyzet</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Point go = (Point)value;
            return new RectangleGeometry(new Rect(go.Y * 10, go.X * 10, 10, 10));
        }

        /// <summary>
        /// rectangle-ből konvertálna popntot
        /// </summary>
        /// <param name="value">a rectangle</param>
        /// <param name="targetType">targettype</param>
        /// <param name="parameter">egyéb paraméter lehet</param>
        /// <param name="culture">culture info</param>
        /// <returns>egy hibát dobna</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
