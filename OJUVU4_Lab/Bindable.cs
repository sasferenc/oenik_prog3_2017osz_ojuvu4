﻿// <copyright file="Bindable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Ojuvu
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// azokhoz kell ahol az megjelenített elemek egy általunk készített osztályhoz van kötve
    /// </summary>
    public class Bindable : INotifyPropertyChanged
    {
        /// <summary>
        /// A változáshoz szükséges event
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// A leszármazottjai hívhatják meg és megnézi hogy van-e olyan elem ami fel van iratkozva rá
        /// </summary>
        /// <param name="prop">a megváltozott tulajdonság neve</param>
        public void OnPropoertyChanged(string prop)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }
    }
}
